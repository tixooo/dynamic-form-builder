import "bootstrap/dist/css/bootstrap.min.css";
import PropTypes from "prop-types";
// eslint-disable-next-line
import React, {Component} from "react";

class TextInput extends Component {
  render() {
    const { label, required, placeholder, value, onChange } = this.props;
    return (
      <div className="form-group">
        <label>
          {label} {required && <span className="text-danger">*</span>}
        </label>
        <input
          type="text"
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
      </div>
    );
  }
}

class Select extends Component {
  render() {
    const { label, required, value, options, onChange } = this.props;
    return (
      <div className="form-group">
        <label>
          {label} {required && <span className="text-danger">*</span>}
        </label>
        <select className="form-control" value={value} onChange={onChange}>
          {options.map((option) => (
            <option key={options.indexOf(option)} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
    );
  }
}

class Checkbox extends Component {
  render() {
    const { label, required, value, onChange } = this.props;
    return (
      <div className="form-check">
        <input
          type="checkbox"
          className="form-check-input"
          checked={value}
          onChange={onChange}
        />
        <label className="form-check-label">
          {label} {required && <span className="text-danger">*</span>}
        </label>
      </div>
    );
  }
}

class DynamicFormCC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {},
      showEmail: false,
    };
  }

  handleChange = (name, value) => {
    this.setState((prevState) => ({
      values: {
        ...prevState.values,
        [name]: value,
      },
    }));
    if (name === "subscribe") {
      this.handleCheckboxChange(value);
    }
  };

  handleCheckboxChange = (value) => {
    if (value) {
      this.setState({
        showEmail: true,
      });
    } else {
      this.setState({
        showEmail: false,
      });
    }
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.values);
    this.setState({ values: {} });
  };
  renderElement = (element) => {
    const { type, id, label, required, placeholder, options } = element;
    const value = this.state.values[id] || "";
    switch (type) {
      case "text":
        return (
          <TextInput
            key={id}
            label={label}
            required={required}
            placeholder={placeholder}
            value={value}
            onChange={(event) => this.handleChange(id, event.target.value)}
          />
        );
      case "select":
        return (
          <Select
            key={id}
            label={label}
            required={required}
            value={value}
            options={options}
            onChange={(event) => this.handleChange(id, event.target.value)}
          />
        );
      case "checkbox":
        return (
          <Checkbox
            key={id}
            label={label}
            required={required}
            value={value}
            onChange={(event) => this.handleChange(id, event.target.checked)}
          />
        );
      default:
        return null;
    }
  };

  render() {
    const elements = this.props.formJSON.fields;
    return (
      <form onSubmit={this.handleSubmit}>
        {elements.map((element) => this.renderElement(element))}
        {this.state.showEmail && (
          <TextInput
            key="email"
            label="Email"
            required={true}
            placeholder="Enter your email"
            value={this.state.values.email || ""}
            onChange={(event) => this.handleChange("email", event.target.value)}
          />
        )}
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    );
  }
}

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};

Checkbox.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

DynamicFormCC.propTypes = {
  formJSON: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default DynamicFormCC;
