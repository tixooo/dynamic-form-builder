import "./App.css";
import formJSON from "./formData.json";
import DynamicFormCC from "./dynamicFormBuilderCC.js";
// eslint-disable-next-line
import React from "react";
const handleFormSubmit = (values) => {
  console.log(values);
};

let App = () => {
  return (
    <div className="container">
      <h1>Dynamic Form Builder</h1>
      <DynamicFormCC formJSON={formJSON} onSubmit={handleFormSubmit} />
    </div>
  );
}
export default App;
